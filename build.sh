#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
OLD_DIR=$PWD
rm -rf build
rm -f ui.tar.gz
npm install -p
npm run build
tar -czvf ui.tar.gz build/*
cd $OLD_DIR