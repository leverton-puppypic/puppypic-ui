import { Image } from 'react-visible-image'
export default class Card extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.up = () => {
      axios.get(`/api/${this.props.id}/up`).then(() => {
        this.props.parent.refresh()
      })
    }
    this.down = () => {
      axios.get(`/api/${this.props.id}/down`).then(() => {
        this.props.parent.refresh()
      })
    }
  }
  render() {
    const parts = this.props.url.replace('http://imgur.com', '').split('.')
    const thumburl = `http://imgur.com/${parts[0]}t.${parts[1]}`
    return (
      <div className='image flex-column'>
        <Image image={thumburl} className='fill flex-center' />
        <div className="field has-addons flex-row">
          <p className="control">
            <a className="button is-primary" onClick={this.up}>
              <span className="icon is-small">
                <i className="fa fa-thumbs-up" />
              </span>
            </a>
          </p>
          <b className='control fill flex-column flex-center score'>
            {this.props.score}
          </b>
          <p className="control">
            <a className="button is-primary" onClick={this.down}>
              <span className="icon is-small">
                <i className="fa fa-thumbs-down" />
              </span>
            </a>
          </p>
        </div>
      </div>
    )
  }
}
