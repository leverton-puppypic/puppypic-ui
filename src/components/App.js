import Card from './Card'
export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.refresh = () => {
      axios.get('/api').then(d => d.data).then(data => {
        this.setState({ data })
      }).catch(console.error)
    }
  }
  componentDidMount() {
    this.refresh()
  }
  render() {
    return (
      <div className='fill flex-column'>
        {
          !this.state.data && (
            <div className='fill flex-column flex-center'>
              <div className='loader' />
            </div>
          )
        }
        {
          this.state.data && (
            <div className='fill flex-flow flex-row flex-center'>
              {
                this.state.data.map(img => {
                  return (
                    <Card {...img} key={img.id} parent={this} />
                  )
                })
              }
            </div>
          )
        }
      </div>
    )
  }
}
