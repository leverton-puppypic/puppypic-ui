import App from '../components/App'
import '../styles/style.sass'
import { AppContainer } from 'react-hot-loader'
const render = () => {
  ReactDOM.render(
    <AppContainer warnings={false}>
      <App />
    </AppContainer>,
    document.getElementById('main')
  )
}
render()
if (module.hot) {
  module.hot.accept(() => render())
}
