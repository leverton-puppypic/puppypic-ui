# PuppyPic UI
This is the user interface for the PuppyPic application. This is a React based web application.

# Input
* You are provided with the source code of the application.
* This is a single page application. As such, all routes (except the ones served by `/api` must be served by this application)
* The application expects to find the API at `/api`
* nodejs and npm are necessary to build the application
* Use `npm run build` to build the application

# Output
* Build artifacts are located in the `build` directory after build

# Notes
* This application will be built with webpack
* Any necessary configuration changes should be made in the `webpack.config.js` file
* Any other changes to the configuration must be documented